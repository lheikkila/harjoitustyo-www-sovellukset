<?php
error_reporting(E_ALL ^ E_DEPRECATED);
?>




<!-- graphics.php -->
<?php
// load header.php with title variable
	$pagetitle = "Graphics";
    include("header.php");
?>



<!-- Content start -->


<h3 align="center">Players's total wins</h3>
<canvas id="canvas"></canvas>

<?php 

//GET PLAYERS FROM DATABASE
mysql_connect('localhost', 'root', '');
mysql_select_db('registration');


$result= mysql_query("SELECT player1 FROM results");
while($row = mysql_fetch_array($result)){
      $values[] = $row['player1'];
 }

 // CREATE TABLE
$new_array=array_count_values($values);

echo "<table id='mydata' class='center' border='0' cellpadding='0'>";
echo	"<tr>       <th>Player</th><th>Total wins</th> </tr>";
while (list ($key, $val) = each ($new_array)) {

echo "<tr><td>$key</td>  <td>$val</td>  </tr><br>";
}
echo "</table>";
?>

<script>

// CREATE PIE CHART

var data_table = document.getElementById('mydata');
var canvas = document.getElementById('canvas');
var td_index = 1; 

var tds, data = [], color, colors = [], value = 0, total = 0;
var trs = data_table.getElementsByTagName('tr'); 
for (var i = 0; i < trs.length; i++) {
    tds = trs[i].getElementsByTagName('td'); 
 
    if (tds.length === 0) continue; 
 
    value  = parseFloat(tds[td_index].innerHTML);
    data[data.length] = value;
    total += value;
 
    // random color
    color = getColor();
    colors[colors.length] = color;
    trs[i].style.backgroundColor = color; 
}


var ctx = canvas.getContext('2d');
var canvas_size = [canvas.width, canvas.height];
var radius = Math.min(canvas_size[0], canvas_size[1]) / 2;
var center = [canvas_size[0]/2, canvas_size[1]/2];


var sofar = 0; 

for (var piece in data) {
 
    var thisvalue = data[piece] / total;
 
    ctx.beginPath();
    ctx.moveTo(center[0], center[1]); 
    ctx.arc(  
        center[0],
        center[1],
        radius,
        Math.PI * (- 0.5 + 2 * sofar), 
        Math.PI * (- 0.5 + 2 * (sofar + thisvalue)),
        false
    );
 
    ctx.lineTo(center[0], center[1]); 
    ctx.closePath();
    ctx.fillStyle = colors[piece];    
    ctx.fill();
 
    sofar += thisvalue; 
}

// utility - generates random color
    function getColor() {
        var rgb = [];
        for (var i = 0; i < 3; i++) {
            rgb[i] = Math.round(100 * Math.random() + 155) ; 
        }
        return 'rgb(' + rgb.join(',') + ')';
    }

</script>



<!-- content end -->
		
<?php 
// include footer.php
include("footer.php");
?>