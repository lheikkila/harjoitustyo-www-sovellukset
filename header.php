<?php
// This file is a header template that is used on every page with page title variable


 
// Check if logged in, if not then redirect to login page

if(session_id() == '' || !isset($_SESSION)) {
    // session isn't started
    session_start();
}

	if (!isset($_SESSION['username'])) {
		$_SESSION['msg'] = "You must log in first";
		header('location: login.php');
	}

	if (isset($_GET['logout'])) {
		session_destroy();
		unset($_SESSION['username']);
		header("location: login.php");
	}

?>
<!DOCTYPE html>
<html>
<head>
	<title>
	<?php 
	//show title from variable
	$pagetitle;

	if(isset($pagetitle)){
	 echo $pagetitle;
	} 
	  else {
	echo "Nörttien lautapelikerho";
	}
	?>
	</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

<div class="wrapper">

	<div class="header">
		<h2>	<?php 
	//show title from variable
	$pagetitle;

	if(isset($pagetitle)){
	 echo $pagetitle;
	} 
	  else {
	echo "Nörttien lautapelikerho";
	}
	?></h2>
	</div>

<!-- menu -->
<ul class="menu">
  <li><a href="index.php"><i class="fa fa-home" aria-hidden="true"></i> HOME</a></li>
  <li><a href="addresult.php"><i class="fa fa-gamepad" aria-hidden="true"></i> ADD RESULT</a></li>
  <li><a href="modifyresult.php"><i class="fa fa-eye" aria-hidden="true"></i> VIEW RESULTS</a></li>
  <li><a href="graphics.php"><i class="fa fa-pie-chart" aria-hidden="true"></i> GRAPHICS</a></li>
</ul>
	<div class="content">
	
	<!-- content in other files -->
	
	<!-- wrapper etc. ends in footer.php -->