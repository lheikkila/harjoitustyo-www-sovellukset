<!-- index.php, welcome page -->
<?php
// load header.php with title variable
	$pagetitle = "NÖRTTIEN LAUTAPELIKERHO";
    include("header.php");
?>

<!-- Content start -->


		<!-- notification message -->
		<?php if (isset($_SESSION['success'])) : ?>
			<div class="error success" >
				<h3>
					<?php 
						echo $_SESSION['success']; 
						unset($_SESSION['success']);
					?>
				</h3>
			</div>
			
		<?php endif ?>
		


		<!-- logged in user information -->
		<?php  if (isset($_SESSION['username'])) : ?>
			<p>Welcome <strong><?php echo $_SESSION['username']; ?></strong></p>
			<p> <a href="index.php?logout='1'" style="color: red;">logout</a> </p>
		<?php endif ?>
		

<!-- content end -->
		
<?php 
// include footer.php
include("footer.php");
?>