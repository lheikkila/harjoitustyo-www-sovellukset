<?php
error_reporting(E_ALL ^ E_DEPRECATED);
?>




<!-- modifyresult.php, used to view results of the games and delete them -->
<?php
// load header.php with title variable
	$pagetitle = "View Results";
    include("header.php");
?>

<!-- Content start -->
	
<?php 
// connect to database

mysql_connect('localhost', 'root', '');
mysql_select_db('registration');

$sql = "SELECT id, gamename, player1, player2, player3, player4, player5, date FROM results";
$result = mysql_query($sql);



// display data in table
?>

<p><b>View All</b>
<br></br>

<table border='0' cellpadding='0' >
<tr> <th>ID</th> <th>Gamename</th> <th>Winner</th> <th>Second</th> <th>Third</th> <th>Fourth</th> <th>Loser</th> <th>Date</th>
<?php

// loop through results of database query, displaying them in the table
while($row = mysql_fetch_array( $result )) {

// echo out the contents of each row into a table

echo "<tr>";
echo '<td>' . $row['id'] . '</td>';
echo '<td>' . $row['gamename'] . '</td>';
echo '<td>' . $row['player1'] . '</td>';
echo '<td>' . $row['player2'] . '</td>';
echo '<td>' . $row['player3'] . '</td>';
echo '<td>' . $row['player4'] . '</td>';
echo '<td>' . $row['player5'] . '</td>';
echo '<td>' . $row['date'] . '</td>';
echo '<td><a class="delete" href="delete.php?id=' . $row['id'] . '"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>';
echo "</tr>";

}

?>
</table>
<br>


<p><a href="addresult.php">Add a new result</a></p>

<!-- delete confirmation -->
<script>
$('.delete').click(function(event) {
    event.preventDefault();
    var r=confirm("Are you sure you want to delete?");
    if (r==true)   {  
       window.location = $(this).attr('href');
    }

});
</script>
<!-- content end -->
		
<?php 
// include footer.php
include("footer.php");
?>
