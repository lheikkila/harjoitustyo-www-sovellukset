<?php 
	session_start();

	// variable declaration
	$username = "";
	$email    = "";
	$errors = array(); 
	$_SESSION['success'] = "";
	$gamename ="";
	$date = "";
	$player1 = "";
	$player2 = "";
	$player3 = "";
	$player4 = "";
	$player5 = "";

	// connect to database
	$db = mysqli_connect('localhost', 'root', '', 'registration');

	// REGISTER USER
	if (isset($_POST['reg_user'])) {
		// receive all input values from the form
		$username = mysqli_real_escape_string($db, $_POST['username']);
		$email = mysqli_real_escape_string($db, $_POST['email']);
		$password_1 = mysqli_real_escape_string($db, $_POST['password_1']);
		$password_2 = mysqli_real_escape_string($db, $_POST['password_2']);

		// form validation: ensure that the form is correctly filled
		if (empty($username)) { array_push($errors, "Username is required"); }
		if (empty($email)) { array_push($errors, "Email is required"); }
		if (empty($password_1)) { array_push($errors, "Password is required"); }

		if ($password_1 != $password_2) {
			array_push($errors, "The two passwords do not match");
		}

		// register user if there are no errors in the form
		if (count($errors) == 0) {
			$password = md5($password_1);//encrypt the password before saving in the database
			$query = "INSERT INTO users (username, email, password) 
					  VALUES('$username', '$email', '$password')";
			mysqli_query($db, $query);

			$_SESSION['username'] = $username;
			$_SESSION['success'] = "You are now logged in";
			header('location: index.php');
		}

	}


	// LOGIN USER
	if (isset($_POST['login_user'])) {
		$username = mysqli_real_escape_string($db, $_POST['username']);
		$password = mysqli_real_escape_string($db, $_POST['password']);

		if (empty($username)) {
			array_push($errors, "Username is required");
		}
		if (empty($password)) {
			array_push($errors, "Password is required");
		}

		if (count($errors) == 0) {
			$password = md5($password);
			$query = "SELECT * FROM users WHERE username='$username' AND password='$password'";
			$results = mysqli_query($db, $query);

			if (mysqli_num_rows($results) == 1) {
				$_SESSION['username'] = $username;
				$_SESSION['success'] = "You are now logged in";
				header('location: index.php');
			}else {
				array_push($errors, "Wrong username/password combination");
			}
		}
	}
	
// ADD RESULT

	if (isset($_POST['add_result'])) {
		// receive all input values from the form
		$gamename = mysqli_real_escape_string($db, $_POST['gamename']);
		$player1 = mysqli_real_escape_string($db, $_POST['player1']);
		$player2 = mysqli_real_escape_string($db, $_POST['player2']);
		$player3 = mysqli_real_escape_string($db, $_POST['player3']);
		$player4 = mysqli_real_escape_string($db, $_POST['player4']);
		$player5 = mysqli_real_escape_string($db, $_POST['player5']);
		$date = mysqli_real_escape_string($db, $_POST['date']);

		// form validation: ensure that the form is correctly filled
		if (empty($player1)) { array_push($errors, "Winner is required"); }
		if (empty($player2)) { array_push($errors, "Second player is required"); }
		if (empty($player3)) { array_push($errors, "Third player is required"); }
		if (empty($player4)) { array_push($errors, "Fourth player is required"); }
		if (empty($player5)) { array_push($errors, "Loser is required"); }
		if (empty($gamename)) { array_push($errors, "Game is required"); }
		if (empty($date)) { array_push($errors, "Date is required"); }


		// insert data into database if there are no errors in the form
		if (count($errors) == 0) {
			$query = "INSERT INTO results (gamename, player1, player2, player3, player4, player5, date) 
					  VALUES('$gamename', '$player1', '$player2', '$player3', '$player4', '$player5', '$date')";
			mysqli_query($db, $query);
			
			$_SESSION['success'] = "Result added";

			header('location: index.php');
		}

	}
	


?>